/**************************************************************************
  *  Token Extra
  *  
  *  README.txt
  **************************************************************************/



Introduction
==========================

This module provides extra token replacements for strings, dates, and numbers.


Usage
==========================

// Number replacement
token_replace($original, 'number', 123);

// String replacement
token_replace($original, 'string', 'hello world');

// Date replacement
token_replace($original, 'date', time());


For more information, please see:

http://drupal.org/handbook/modules/token

